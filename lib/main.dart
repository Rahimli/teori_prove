import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:teori_prove/authentication/auth.dart';
import 'package:teori_prove/authentication/auth_provider.dart';
import 'package:teori_prove/authentication/roots/root-page.dart';

import 'home/home_page.dart';


void main() {
//  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//      statusBarColor: Colors.white,
//    statusBarBrightness:
//  ));
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
    statusBarColor: Color(0xff3F458F), //or set color with: Color(0xFF0000FF)
    statusBarIconBrightness: Brightness.dark,
    statusBarBrightness: Brightness.light,
  ));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(App());
}


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AuthProvider(
      auth: Auth(),
      child: MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.white,
        appBarTheme: AppBarTheme(
          // iconTheme: IconThemeData(color: Colors.black)
          // color: Colors.white,

          actionsIconTheme: IconThemeData(color: Colors.black),
        )
      ),
      debugShowCheckedModeBanner: false,
        home: MainRootPage(),
      ),
    );
  }
}


//
//class App extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      theme: ThemeData(
//        primaryColor: Colors.white,
//        appBarTheme: AppBarTheme(
//          // iconTheme: IconThemeData(color: Colors.black)
//          // color: Colors.white,
//
//          actionsIconTheme: IconThemeData(color: Colors.black),
//        )
//      ),
//      debugShowCheckedModeBanner: false,
//      home: MainRootPage(),
//    );
//  }
//}

//void main() {
//  BlocSupervisor().delegate = SimpleBlocDelegate();
//  runApp(App(userRepository: UserRepository()));
//}

//class App extends StatefulWidget {
//  final UserRepository userRepository;
//
//  App({Key key, @required this.userRepository}) : super(key: key);
//
//  @override
//  State<App> createState() => _AppState();
//}
//
//class _AppState extends State<App> {
//  AuthenticationBloc authenticationBloc;
//  UserRepository get userRepository => widget.userRepository;
//
//  @override
//  void initState() {
//    authenticationBloc = AuthenticationBloc(userRepository: userRepository);
//    authenticationBloc.dispatch(AppStarted());
//    super.initState();
//  }
//
//  @override
//  void dispose() {
//    authenticationBloc.dispose();
//    super.dispose();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      theme: ThemeData(
//        primaryColor: Colors.white,
//        appBarTheme: AppBarTheme(
//          // iconTheme: IconThemeData(color: Colors.black)
//          // color: Colors.white,
//
//          actionsIconTheme: IconThemeData(color: Colors.black),
//        )
//      ),
//      debugShowCheckedModeBanner: false,
//        home: HomePage(),
//      );
//  }
//}
//
