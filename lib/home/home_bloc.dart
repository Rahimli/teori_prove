//import 'package:rxdart/rxdart.dart';
//import 'package:teori_prove/models/category.dart';
//import 'package:teori_prove/repository/repository.dart';
//
//class HomeBloc {
//  final Repository _repository = Repository();
//  final BehaviorSubject<CategoryData> _subject = BehaviorSubject<CategoryData>();
//
//
//  fetchAllCategories() async {
//    CategoryData categoryData = await _repository.fetchAllCategories();
//    // print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
//    // print("--- home category-data ---");
//    _subject.sink.add(categoryData);
//  }
//
//  dispose() {
//    _subject.close();
//  }
//  BehaviorSubject<CategoryData> get allHomeCategories => _subject;
//}
//
//final home_bloc = HomeBloc();


import 'package:teori_prove/models/app-tariff-type.dart';
import 'package:teori_prove/models/category.dart';
import 'package:teori_prove/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<CategoryData> _subject =
  BehaviorSubject<CategoryData>();
  final BehaviorSubject<AppTariffType> _subjectTarifType =
  BehaviorSubject<AppTariffType>();

  getCategories() async {
    CategoryData categoryData = await _repository.fetchAllCategories();
//    // print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
//    // print("--- home category-data ---");
    _subject.sink.add(categoryData);
    AppTariffType appTariffTypeData = await _repository.fetchAppTariffType();
    _subjectTarifType.sink.add(appTariffTypeData);

//    // print(appTariffTypeData.appTariffType.toString());
//    // print("appTariffTypeData.appTariffType.toString()");
  }

  getTariffType() async {
    AppTariffType appTariffTypeData = await _repository.fetchAppTariffType();
//    // print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
//    // print("--- home appTariffType-data ---");
    _subjectTarifType.sink.add(appTariffTypeData);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<CategoryData> get subject => _subject;
  BehaviorSubject<AppTariffType> get subjectTarifType => _subjectTarifType;

}

final home_bloc = HomeBloc();