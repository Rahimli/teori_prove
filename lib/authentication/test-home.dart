import 'package:flutter/material.dart';
import 'package:teori_prove/authentication/auth.dart';
import 'package:teori_prove/authentication/auth_provider.dart';

class HomePage extends StatelessWidget {
  const HomePage(this.onSignedOut,this.onSignedIn, this.loggid);
  final VoidCallback onSignedOut;
  final VoidCallback onSignedIn;
  final int loggid ;

  Future<void> _signOut(BuildContext context) async {
    try {
      final BaseAuth auth = AuthProvider.of(context).auth;
      await auth.signOut();
      onSignedOut();
    } catch (e) {
      // print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome'),
        actions: <Widget>[
          FlatButton(
            child: Text('Logout', style: TextStyle(fontSize: 17.0, color: Colors.white)),
            onPressed: () => _signOut(context),
          )
        ],
      ),
      body: Container(
        child: Center(child: Text('Welcome ${loggid.toString()}', style: TextStyle(fontSize: 32.0))),
      ),
    );
  }
}