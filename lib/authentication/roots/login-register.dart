
import 'package:flutter/material.dart';
import 'package:teori_prove/authentication/pages/login_page.dart';


import 'package:teori_prove/authentication/auth.dart';
import 'package:teori_prove/authentication/auth_provider.dart';
import 'package:teori_prove/authentication/pages/register_page.dart';
import 'package:teori_prove/home/home_page.dart';
//import 'package:teori_prove/authentication/test-home.dart';
//import 'package:teori_prove/authentication/test-home.dart';
import 'package:teori_prove/pages/loading.dart';






class LoginRegisterMain extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginRegisterMainState();
}

enum AuthStatus {
  notDetermined,
  notSignedIn,
  signedIn,
}

class _LoginRegisterMainState extends State<LoginRegisterMain> {
  AuthStatus authStatus = AuthStatus.notDetermined;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final BaseAuth auth = AuthProvider.of(context).auth;
    auth.currentUser().then((int userId) {
      setState(() {
        authStatus = userId == null ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      });
    });
  }

  void _signedIn() {
    setState(() {
      authStatus = AuthStatus.signedIn;
    });
  }

  void _signedOut() {
    setState(() {
      authStatus = AuthStatus.notSignedIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.notDetermined:
        return _buildWaitingScreen();
      case AuthStatus.notSignedIn:
        return LoginRegister(
          onSignedIn: _signedIn,
        );
      case AuthStatus.signedIn:
        return HomePage(
          _signedOut,
          1,
        );
    }
    return null;
  }

  Widget _buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: LoadingPage(),
      ),
    );
  }
}

class LoginRegister extends StatelessWidget {
  const LoginRegister({this.onSignedIn});
  final VoidCallback onSignedIn;

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery
        .of(context)
        .padding
        .top;

    return Scaffold(
      backgroundColor: Colors.purple,
      body: Container(
        padding: new EdgeInsets.only(top: statusbarHeight),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/main_background1.png"),fit: BoxFit.cover
            )
        ),
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/main_background.png"),fit: BoxFit.cover
              )
          ),
          child: Stack(

            children: <Widget>[
              Align(
                  alignment: Alignment(0, -0.85),
                  child: Image.asset("assets/images/logo.png",height: 115,)
              ),
              Align(
                  alignment: Alignment(0, 0.6),
                  child: Container(
                    child: Text("Text view", style: TextStyle(color: Colors.white),),
                  )
              ),
              Align(
                alignment: Alignment(0, 0.9),
                child: Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => LoginPage(onSignedIn: onSignedIn,)),
                          );
                        },
                        child: Text("Sign in",style: TextStyle(color: Colors.deepPurple),),
                        color: Colors.white, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),padding: EdgeInsets.all(14.0),
                      ),
                      SizedBox(width: 20.0,),
                      RaisedButton(onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => RegisterPage(onSignedIn: onSignedIn,)),
                        );
                      },
                        child: Text("Create Account",style: TextStyle(color: Colors.deepPurple)),color: Colors.white, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),padding: EdgeInsets.all(14.0),),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
