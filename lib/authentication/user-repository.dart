import 'dart:async';
import 'package:meta/meta.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:teori_prove/models/user-repository.dart';



class UserRepository {

  Future<UserModel> authenticate({@required String email,@required String password,}) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      Dio dio = new Dio();
      // await Future.delayed(Duration(seconds: 1));
      Response response = await dio.post("http://test.azweb.dk/api/auth/login", data: {"email": email.trim(), "password": password.trim()});
      // // print(response);
      // // print(response.statusCode);

      UserModel userModel = UserModel();
      if(response.statusCode == 200) {
        userModel = UserModel.fromJson(response.data);

        prefs.setString('access_token', userModel.accessToken.toString());
//        token = response.data['access_token'];
        // // print(response.data.toString()['access_token']);
      }else{
        // print("zad");
//        userModel = null;
      }
      // print("-------------------------------------------");
//      // print(prefs.getString('access_token'));
      // print("********************************************");
//      // print(token.toString());


      return userModel;
  }


  Future<void> deleteToken() async {
    /// delete from keystore/keychain

    SharedPreferences prefs = await SharedPreferences.getInstance();

    Dio dio = new Dio();
    // await Future.delayed(Duration(seconds: 1));
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' :'Bearer ' + prefs.getString('access_token').toString(),
    };
    dio.options.headers = headersMap;
    Response response = await dio.post("http://test.azweb.dk/api/auth/logout");
    // print("bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-");
    if (response.statusCode == 200){
      prefs.setString('access_token', null);
    }
    // print("_____________________________-----------------------------------");
    // print("bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-bla-");
    // print(response.data['access_token']);
    return;
  }

  Future<void> persistToken(String token) async {
    /// write to keystore/keychain
    Dio dio = new Dio();
    // await Future.delayed(Duration(seconds: 1));
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' :'Bearer ' + token,
    };
    dio.options.headers = headersMap;
    Response response = await dio.post("http://test.azweb.dk/api/auth/me");
    // // print(response.statusCode);
    if(response.statusCode == 200) {
      token = response.data['access_token'];
      // // print(response.data.toString()['access_token']);
    }else{
      // print("zad");
    }



    return;
    // return;
  }

  Future<bool> checkToken(String token) async {
    /// write to keystore/keychain
    Dio dio = new Dio();
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + token,
    };
    try{
      dio.options.headers = headersMap;
      Response response = await dio.post("http://test.azweb.dk/api/auth/me");
      token = response.data['access_token'];
      return true;
    }catch(exception){
      return false;
    }



  }

  Future<bool> hasToken() async {
    // / read from keystore/keychain
    // /
    // print("has token ");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String access_token = prefs.getString('access_token');
    if (access_token != null){
      // print("first_if");
      if(await checkToken(access_token)){
        // print('second if');
        return true;
      }
      // print(access_token);
      // return true;
      // print("first is end ");
    }

    // print("%%^^^^^^^^^^^^^^^^--------------------------^^^^^^");
    return false;
  }




  Future<UserModel> signUp({String name, String email, String password}) async {
    Dio dio = new Dio();
    // await Future.delayed(Duration(seconds: 1));
    var dataHub = {
      "name": name.trim(),
      "email": email.trim(),
      "password": password.trim(),
      "password_confirmation":password.trim()
    };
    Response response = await dio.post("http://test.azweb.dk/api/auth/register", data: dataHub);

    UserModel userModel = UserModel();

    if(response.statusCode == 200) {
      // token = response.data['access_token'];
      userModel = UserModel.fromJson(response.data);
      // print(response.data['access_token']);
      return userModel;
    }else{
      // print("zad");
      return userModel;
    }
  }

  Future<bool> isSignedIn() async {
    // / read from keystore/keychain
    // /
    // print("has token ");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String access_token = prefs.getString('access_token');
    if (access_token != null){
      // print("first_if");
      if(await checkToken(access_token)){
        // print('second if');
        return true;
      }
      // print(access_token);
      // return true;
      // print("first is end ");
    }

    // print("%%^^^^^^^^^^^^^^^^--------------------------^^^^^^");
    return false;
  }

  Future<UserModel> getUser() async {
    // print("lalalaallallallalalalalalalaallallallalalala");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String access_token = prefs.getString('access_token').toString();
    Dio dio = new Dio();
    Map<String, String> headersMap = {
      'Content-Type' : 'application/json',
      'Authorization' : 'Bearer ' + access_token,
    };

    // print("^^^^^^^^^^&&&&&&&&&&&&&&&&&&&&&&*****************");
    // print(access_token);
    // print("^^^^^^^^^^&&&&&&&&&&&&&&&&&&&&&&*****************");

    UserModel userModel = UserModel();

    try{
      dio.options.headers = headersMap;
      Response response = await dio.post("http://test.azweb.dk/api/auth/me");
//      if(response.statusCode == 200) {
        userModel = UserModel.fromJson(response.data);
//      }
      // print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      // print(userModel.id);
      return userModel;
    }catch(exception){

      // print("*********************** catch ${exception.toString()} *******************************");
      return userModel;
    }
  }
}