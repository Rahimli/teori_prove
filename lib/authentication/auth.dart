import 'dart:async';

import 'package:teori_prove/authentication/user-repository.dart';
import 'package:teori_prove/models/user-repository.dart';

abstract class BaseAuth {
  Future<String> signInWithEmailAndPassword(String email, String password);
  Future<String> createUserWithEmailAndPassword(String name, String email, String password);
  Future<int> currentUser();
  Future<UserModel> currentUserDetail();
  Future<void> signOut();
}

class Auth implements BaseAuth {
  UserRepository userRepository = UserRepository();
  @override
  Future<String> signInWithEmailAndPassword(String email, String password) async {
    final UserModel userModel = await userRepository.authenticate(email: email.trim(), password: password.trim());
    return userModel?.id.toString();
  }

  @override
  Future<String> createUserWithEmailAndPassword(String name, String email, String password) async {
    final UserModel userModel = await userRepository.signUp(name: name.trim(),email: email.trim(),password: password.trim());
    return userModel?.id.toString();
  }

  @override
  Future<int> currentUser() async {
    // // print("----------------rarararrararrararararrara");
    final UserModel userModel = await userRepository.getUser();
    // // print("nese userModel?.id.toString()userModel?.id.toString() = ${userModel?.id.toString()}");
    return userModel?.id;
  }

  @override
  Future<void> signOut() async {
    return userRepository.deleteToken();
  }

  @override
  Future<UserModel> currentUserDetail() async{
    // // print("----------------rarararrararrararararrara");
    final UserModel userModel = await userRepository.getUser();
    // // print("nese userModel?.id.toString()userModel?.id.toString() = ${userModel?.id.toString()}");
    return userModel;
  }
}