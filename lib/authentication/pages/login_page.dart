import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:teori_prove/authentication/auth.dart';
import 'package:teori_prove/authentication/auth_provider.dart';
import 'package:teori_prove/authentication/common/common.dart';
import 'package:teori_prove/authentication/roots/login-register.dart';

class EmailFieldValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Email can\'t be empty' : null;
  }
}

class PasswordFieldValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Password can\'t be empty' : null;
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({this.onSignedIn});
  final VoidCallback onSignedIn;

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

enum FormType {
  login,
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 18.0,color: Colors.white);

  String _email;
  String _password;
  FormType _formType = FormType.login;

  bool validateAndSave() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  goToHome() {
    final page = AuthProvider(
      auth: Auth(),
      child: LoginRegisterMain(),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }
  void _showFormError(BuildContext context,String message) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Error"),
          content: Text("${message}"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        )
    );
  }

  Future<void> validateAndSubmit() async {

    

    if (validateAndSave()) {
      LoadingDialog.showFormLoadingDialog(context, "Please wait");
      try {
        final BaseAuth auth = AuthProvider.of(context).auth;
        if (_formType == FormType.login) {
          final String userId = await auth.signInWithEmailAndPassword(_email, _password);
          // // print('Signed in: $userId');
          Navigator.pop(context);
          if (userId != null){
            goToHome();
          }
        }
        widget.onSignedIn();
      } on DioError catch(e) {
        if(e.response.statusCode == 401){
          // // print("Error ${e.response.statusCode}");
          Navigator.pop(context);
          _showFormError(context,"Email or Password is incorrect");
        }
      }finally{
//        Navigator.pop(context);
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: MediaQuery.of(context).size.height,
//                    maxHeight: MediaQuery.of(context).size.width,
                  ),
                  child: new Container(
//                  height: 300.0,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        fit: BoxFit.cover,
                        image: new AssetImage("assets/images/main_background1.png"),
                      ),
                    ),
                    child: Container(
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          fit: BoxFit.cover,
                          image: new AssetImage("assets/images/main_background2.png"),
                        ),
                      ),
                      child: Form(
                         key: formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AppBar(
                              title: Text("Signin",style: TextStyle(color: Colors.black),),
                              centerTitle: true,
                              elevation: 0.0,
                              backgroundColor: Colors.transparent,
                            ),
                            SizedBox(
                              height: 170.0,
                            ),
                            SizedBox(height: 45.0),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 4),
                              child: Text("Email",style: TextStyle(color: Colors.white),),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 5.0, bottom: 5.0, left: 20.0, right: 20.0),
                              child: TextFormField(
                                key: Key('email'),
                                validator: EmailFieldValidator.validate,
                                onSaved: (String value) => _email = value,
                                // validator: validator.validateEmail,
                                obscureText: false,
                                style: style,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Color(0xff3F458F),
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                      borderSide: BorderSide(width: 0)),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(width: 0)),
                                ),
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 4),
                              child: Text("Password",style: TextStyle(color: Colors.white),),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 5.0,
                                  bottom: 10.0,
                                  left: 20.0,
                                  right: 20.0),
                              child: TextFormField(
                                key: Key('password'),
                                validator: PasswordFieldValidator.validate,
                                onSaved: (String value) => _password = value,
                                obscureText: true,
                                style: style,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Color(0xff3F458F),
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 11.0, 20.0, 11.0),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                      borderSide: BorderSide(width: 0)),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(width: 0)),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 25.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 10.0,
                                  bottom: 10.0,
                                  left: 20.0,
                                  right: 20.0),
                              child: Material(
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(30.0),
                                color: Colors.white,
                                child: MaterialButton(
                                  key: Key('signIn'),
                                  onPressed: validateAndSubmit,
                                  minWidth: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                                  child: Text( 'Continue',
                                      textAlign: TextAlign.center,
                                      style: style.copyWith(
                                          color: Color(0xff3F458F),
                                          fontWeight: FontWeight.w400,
                                          fontSize: 15.0)
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
    );
  }
}