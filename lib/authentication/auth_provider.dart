import 'package:flutter/material.dart';
import 'package:teori_prove/authentication/auth.dart';

class AuthProvider extends InheritedWidget {
  final BaseAuth auth;

//  AuthProvider(this.auth);
  const AuthProvider({Key key, Widget child, this.auth}) : super(key: key, child: child);


  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static AuthProvider of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AuthProvider);
  }
}