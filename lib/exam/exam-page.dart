import 'dart:async';
import 'dart:collection';
import 'package:random_string/random_string.dart' as random;
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';

import 'package:flutter/material.dart';
import 'package:teori_prove/authentication/common/common.dart';
import 'package:teori_prove/models/question.dart';
import 'package:teori_prove/models/user-repository.dart';
import 'package:teori_prove/pages/loading.dart';
import 'package:teori_prove/pages/loading2.dart';
import 'package:teori_prove/pages/no-data.dart';
import 'package:teori_prove/statistics/detail/detail_page.dart';
import 'package:teori_prove/statistics/detail/statistic_detail_provider.dart';

import 'category_detail_bloc_provider.dart';
import 'exam_bloc.dart';
import 'dart:core';
import 'dart:math';


import 'package:teori_prove/authentication/auth.dart';
import 'package:teori_prove/authentication/auth_provider.dart';


class ExamPage extends StatefulWidget {
  final List<int> ids;
  final bool is_open;

  const ExamPage({this.ids, this.is_open});
  @override
  _ExamPageState createState() {
    return _ExamPageState(
      ids:ids,
    );
  }
}

class _ExamPageState extends State<ExamPage> {

  final List<int> ids;

  AudioPlayer audioPlayer =  AudioPlayer();
  AudioCache audioCache = AudioCache();
  UserModel currentUser = UserModel();
  _ExamPageState({this.ids});

  ExamPageBloc bloc;
  bool examFinish = true;
  int queston_index = -1;
  int answer_index = 0;
  String music_url = '';
  bool showNext = false;
  bool answer_index_option_show = false;
  String session_id;
  Map<String, int> _radioValued =  new HashMap();
//  List<Map>

//  ScrollController _scrollController = new ScrollController();

  static const chars = "abcdefghijklmnopqrstuvwxyz0123456789";

  String RandomString(int strlen) {
    Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
    String result = "";
    for (var i = 0; i < strlen; i++) {
      result += chars[rnd.nextInt(chars.length)];
    }
    return result;
  }

  @override
  void initState() {
    for (int y = 0; y < 100; y++) {
      for (int x = 0; x < 10; x++) {
      _radioValued['${y}-${x}'] = -1;
      }
    }
    session_id = RandomString(20);
//    session_id = random.randomString(15);
    super.initState();

  }

  int _radioValue1 = -1;
  @override
  void didChangeDependencies() {
    bloc = ExamPageBlocProvider.of(context);
    bloc.getQuestions(ids);
    // // print("recreated");
    queston_index+=1;
    if(widget.is_open){
      examFinish = false;
    }
    // // print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5");
    // // print(examFinish);
//    _initAudioPlayer();



    final BaseAuth auth = AuthProvider.of(context).auth;
    // // print("^^^^^^^^^^^^^^^^^^^^^^^^^^");
    auth.currentUserDetail().then((UserModel user) {
      // // print("%---------------------------------------------******************************#");

      setState(() {
        if(user.id != null){
          currentUser = user;
          if(user.paymentType != 'free' || widget.is_open){
            examFinish = false;
          }
        }
//        authStatus = userId == null  ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      });
      // // print(currentUser.id);
    }).catchError((onError){
      // // print("%******************************---------------------------------------------#");

      // // print(onError.toString());
    });

    // // print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5");
    // // print(examFinish);

    super.didChangeDependencies();

  }

  int correctScore = 0;

//  @override
//  void dispose() {
//    bloc.dispose();
//    // // print("~~~~~~~~~~~~~~~~~~~~~~~~~ dispose ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//    super.dispose();
//    // // print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//
//  }

  ScrollController _scrollController = new ScrollController();

  void nextQuestion(){
    setState(() {
      queston_index += 1;
      answer_index = 0;
      showNext = false;
      answer_index_option_show = false;

    });
  }

  Map<String,int> my_answer_map = {};
  List<int> question_list = [];

  Future play(String audioURL) async {

    AudioPlayer curPlayer = AudioPlayer();
    int result = await curPlayer.play(audioURL);
    if (result == 1) {
      // success
    }
    curPlayer.onPlayerCompletion.listen((event) async{
      // // print('audioPlayer.onPlayerCompletion');
      await nextSong();
      nextAnswer();
    });

  }
  Future playFirst(String audioURL) async {
    AudioPlayer questionPlayer = AudioPlayer();

    int result = await questionPlayer.play(audioURL);
    if (result == 1) {
      // success
    }
    questionPlayer.onPlayerCompletion.listen((event) {
      // // print('audioPlayer.onPlayerCompletion');
      nextSong();
//      answer_index_option_show = true;
      setState(() {
        answer_index = 1;
      });
    });
  }
  Future<void> only_play(String audioURL) async {
//    await audioPlayer.stop();
//    await audioPlayer.setReleaseMode(ReleaseMode.STOP);

//    await audioPlayer.setReleaseMode(ReleaseMode.RELEASE);
//    AudioPlayer player = AudioPlayer();
//    audioPlayer.setUrl(audioURL);
    // // print("------------ 54875485784758457847584758748574854858457 ------------");
    // // print(music_url);

    AudioPlayer curPlayer = AudioPlayer();
    int result = await curPlayer.play(audioURL);
    if (result == 1) {
      // // print("~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!");
    }else{
      // // print("(*(*)*)(*(*)*)(*(*)*)(*(*)*)(*(*)*)(*(*)*)(*(*)*)(*(*)*)(*(*)*)(*(*)*)(*(*)*)");
    }
    curPlayer.onPlayerCompletion.listen((event){
      setState(() {
        answer_index_option_show = true;
      });
      // // print("<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<>>>>>>>>>>>>>>>");
    });
  }
//  next_play(String audioURL) async {
//    int result = await audioPlayer.play(audioURL);
//    // // print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
//    // // print(result);
//    if (result == 1) {
//      // success
//    }
//  }
  void playMusicFirst(String url,String next_url) async{
    // // print("--------- play music ------ ");
//    nextQuestion();
//    try {
      await playFirst(url);
//    }catch(e){
//      // // print("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
//    }
    // // print("--------- play music 2------ ");
    music_url = next_url;
    // // print(url);
    // // print(music_url);
//    await nextSong();
//    nextAnswer();

  }



  void playAnswerSelect(String url,String next_url) async{
    // // print("--------- playAnswerSelect music ------ ");
//    nextQuestion();
//    try {
      await play(url);
//    }catch(e){
//      // // print("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
//    }
    // // print("--------- playAnswerSelect music 2------ ");
      music_url = next_url;
      // // print(url);
      // // print(music_url);
//        await nextSong();

//      nextAnswer();
  }


  Future play1(String audioURL) async {
    int result = await audioPlayer.play(audioURL);
    if (result == 1) {
      // success
    }
    audioPlayer.onPlayerCompletion.listen((event) {
      // // print('audioPlayer.onPlayerCompletion');
      nextSong();
      nextAnswer();
    });

  }






  nextSong() async {
    // // print("next next next next next next next next next next next next next next next next ");
    // // print(music_url);

//    await audioPlayer.stop();
    await only_play(music_url);
  }
  void nextAnswer(){
//    setState(() {
      // // print("yeaaaaaaahhhhhhhhhhhhyeaaaaaaahhhhhhhhhhhhyeaaaaaaahhhhhhhhhhhh");
      setState(() {
        answer_index += 1;
      });
      // // print(answer_index);
//    });
  }


  void _handleRadioValueChange1(int answerId,int value,int index, String currentAudioUrl, String nextAudioUrl, bool hasNext) {
    // // print(_radioValue1);
    // // print(answer_index);
    // // print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    
    setState(() {
      Timer(Duration(milliseconds: 300), () => _scrollController.jumpTo(_scrollController.position.maxScrollExtent));
//      _scrollController.animateTo(_scrollController.position.maxScrollExtent, duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      if(hasNext){
        // // print("has next has next has next has next has next has next has next has next has next ");
        if (index >= answer_index-1  ){
          showNext = false;
//        play(audioUrl);
          answer_index += 1;
          answer_index_option_show = false;
          music_url = nextAudioUrl;
          nextSong();
          // // print(currentAudioUrl);
          // // print(music_url);
          // // print('zad zad zad zad zad zad zad zad zad zad zad zad zad zad');
        }else{
//          showNext = showNext;
//          answer_index = 0;

          // // print("noooooooooooooo noooooooooooooo noooooooooooooo noooooooooooooo noooooooooooooo ");
        }
      }else{
        showNext = true;
        // // print("has current has current has current has current has current has current has current ");
      }
      _radioValued["${queston_index}-${index}"] = value;
      _radioValue1 = value;
      // // print(_radioValue1);
      // // print(correctScore);
      String key = "${answerId}";
      switch (_radioValue1) {
        case 0:
//          Fluttertoast.showToast(msg: 'Correct !',toastLength: Toast.LENGTH_SHORT);
          correctScore++;
          my_answer_map[key] = 0;
          break;
        case 1:
          my_answer_map[key] = 1;
//          Fluttertoast.showToast(msg: 'Try again !',toastLength: Toast.LENGTH_SHORT);
          break;
        case -1:
//          Fluttertoast.showToast(msg: 'reset !',toastLength: Toast.LENGTH_SHORT);
          break;
      }
    });
    // // print("==============================================================");
    // // print(my_answer_map);
    // // print("==============================================================");
  }



  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit this Exam'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: roundedButton("No", const Color(0xff282d5a),
                const Color(0xFFFFFFFF)),
          ),
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(true),
            child: roundedButton(" Yes ", const Color(0xff282d5a),
                const Color(0xFFFFFFFF)),
          ),
        ],
      ),
    ) ??
        false;
  }


  Future<bool> _onFinishExamDialog() {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
      return SimpleDialog(
        title: const Text('Finish current exam session'),
        children: <Widget>[
          SimpleDialogOption(

            onPressed: () {
//              Navigator.pop(context);
//              Navigator.push(
//                context,
//
//                MaterialPageRoute(builder: (context) => CategoriesPage()),
//              );
            },
            child:  Padding(
              padding: const EdgeInsets.only(top: 0,bottom: 13.0),
              child: Text('Are you sure to finish current exam session?',style: TextStyle(fontSize: 16.0),),
            ),
          ),
//          SimpleDialogOption(
//
//            onPressed: () {},
//            child:  Padding(
//              padding: const EdgeInsets.only(top: 6,bottom: 6.0),
//              child: Text('NEXT EXAM',style: TextStyle(color: Colors.pink,fontSize: 14.0, fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
//            ),
//          ),
          SimpleDialogOption(
            onPressed: () {
              Navigator.pop(context);
//                  goSignIn();
            },
            child: Padding(
                padding: const EdgeInsets.only(top: 6,bottom: 6.0),
                child: Text('CANCEL',style: TextStyle(color: Colors.pink,fontSize: 14.0, fontWeight: FontWeight.w600),textAlign: TextAlign.right)
            ),
          ),
          SimpleDialogOption(
            onPressed: () {
              Navigator.pop(context);
                  finishExam();
            },
            child: Padding(
                padding: const EdgeInsets.only(top: 6,bottom: 6.0),
                child: Text('FINISH EXAM',style: TextStyle(color: Colors.pink,fontSize: 14.0, fontWeight: FontWeight.w600),textAlign: TextAlign.right)
            ),
          ),
        ],
      );
    });
  }

  Widget roundedButton(String buttonLabel, Color bgColor, Color textColor) {
    var loginBtn = new Container(
      padding: EdgeInsets.all(5.0),
      alignment: FractionalOffset.center,
      decoration: new BoxDecoration(
        color: bgColor,
        borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: const Color(0xFF696969),
            offset: Offset(1.0, 6.0),
            blurRadius: 0.001,
          ),
        ],
      ),
      child: Text(
        buttonLabel,
        style: new TextStyle(
            color: textColor, fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    );
    return loginBtn;
  }


  @override
  Widget build(BuildContext context) {
    return
      WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          
          appBar: AppBar(
            actions: <Widget>[
              queston_index > 0 || answer_index > 1 ?
              IconButton(icon: Icon(Icons.directions),
                onPressed: (){
                  _onFinishExamDialog();
                }
              ):SizedBox(width: 0,),
            ],
            title: Text("Question ${(queston_index+1).toString()}"),
          ),
          body:
//        Column(
//          children: <Widget>[
//            Container(
//              child:
                  StreamBuilder<QuestionData>(
                    stream: bloc.subject.stream,
                    builder: (context, AsyncSnapshot<QuestionData> snapshot) {
                      if (snapshot.hasData) {
                        if (snapshot.data.error != null && snapshot.data.error.length > 0){
                          return ErrorData(snapshot.data.error);
                        }

                        if(question_list.length==0){
                          for (var i=0; i< snapshot.data.questions.length; i+=1){
                            if (!question_list.contains(snapshot.data.questions[i].id)){
                              question_list.add(snapshot.data.questions[i].id);
                            }
                          }
                        }

                        return _buildMainWidget(snapshot.data);

                      } else if (snapshot.hasError) {
                        return ErrorData(snapshot.error);
                      } else {
                        return LoadingPage();
                      }
                    },
                  )
//            ),
//          ],
//        ),
        ),
      );
  }

  List<Widget> questionBox(String imageUrl, String text){
    return [
        Container(
          height: 200.0,

          decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage("${imageUrl}"),
                fit: BoxFit.fill,
              )
        ),
      ),
      // Image.network("${questionData.questions[currentIndex].imageUrl}",fit: BoxFit.fill,),
      Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10,20,10,20),
          child: Text("${text}"),
        ),
      ),
    ];
  }

  _openStatisticDetail(String sessionId) {
    final page = StatisticDetailBlocProvider(
      child: StatisticPageDetail(session_id: sessionId,),
    );
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }


  Future<void> finishExam() async {
      // // print(mounted);
      LoadingDialog.showFormLoadingDialog(context, "Please wait");
      bool result = await bloc.finishExam(session_id,question_list,my_answer_map);
      // // print("resultresultresultresultresultresultresultresultresultresult");
      // // print(result);
      if (result){
          Navigator.pop(context);
        _openStatisticDetail(session_id);
      }else {
        Navigator.pop(context);
        Navigator.of(context).pop(true);
        LoadingDialog.showErrorMessage(context, "Please try again");
      }
  }

  Widget _buildMainWidget(QuestionData data) {


    Questions question_item = data
        .questions[queston_index];
    // // print("******************************   every time works check ******************************   ");
    // // print(answer_index);

    if (answer_index.toInt() == 0) {


      playMusicFirst(

          question_item.audioUrl,
          question_item.subQuestions.length > 0 ? question_item.subQuestions[0].audioUrl : ''
      );
    }else{
      // // print("!!=0");
    };
    return
      Column(
        children: <Widget>[
          Column(
            children: questionBox(data
                .questions[queston_index].imageUrl,
                data
                    .questions[queston_index].text),
          ),

          Expanded(
            child: ListView.builder(
              reverse: false,

              shrinkWrap: true,
              controller: _scrollController,
              itemCount: data
                  .questions[queston_index].subQuestions
                  .length,
              itemBuilder: (BuildContext context,
                  int index) {

                Questions sub_question_item = data.questions[queston_index]
                    .subQuestions[index];
                // // print('................................ ${answer_index.toString() }................');
                return Card(
                  child: Column(
                    children: <Widget>[
                      answer_index > index ?
                      AnimatedOpacity(

                        opacity: answer_index > index ? 1.0 : 0.0,
                        duration: Duration(seconds: 2),
                        child:
                        Padding(
                          padding: const EdgeInsets.fromLTRB(
                              10, 0, 10, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment
                                .spaceBetween,
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
//                                                          flex:84,
//                                                            width: MediaQuery.of(context).size.width * 0.70,
                                  child: Text("${sub_question_item.text}")
                              ),
                              Container(
                                width: 85,
//                                                        Expanded(
//                                                          flex: 30,
                                child: Padding(
                                  padding: const EdgeInsets.all(
                                      0),
                                  child: answer_index > index && answer_index_option_show || ((answer_index -1) > index) ? Column(
                                    // crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceEvenly,
                                    children: <Widget>[

                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment
                                            .center,
                                        children: <Widget>[
                                          Theme(
                                            data: ThemeData(unselectedWidgetColor: Colors.green),
                                            child: Radio(
                                              value: 1,

                                              activeColor: Colors.green,
                                              groupValue: _radioValued['${queston_index}-${index}'],
                                              onChanged: (val) {

                                                if(index == data.questions[queston_index]
                                                    .subQuestions.length - 1) {
                                                  _handleRadioValueChange1(
                                                      sub_question_item
                                                          .id,
                                                      val, index,
                                                      sub_question_item
                                                          .audioUrl,
                                                      '',
                                                      false);
                                                }else{
                                                  _handleRadioValueChange1(
                                                      sub_question_item
                                                          .id,
                                                      val,
                                                      index,
                                                      sub_question_item.audioUrl,
                                                      data.questions[queston_index]
                                                          .subQuestions[index+1].audioUrl,
                                                      true
                                                  );
                                                }
                                              },
                                            ),
                                          ),
                                          Text(
                                            'Ja',
                                            style: TextStyle(
                                                fontSize: 14.0),
                                          ),

                                        ],
                                      ),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment
                                            .center,
                                        children: <Widget>[
                                          Theme(
                                            data: ThemeData(unselectedWidgetColor: Colors.red),
                                            child: Radio(
                                              value: 0,
                                              activeColor: Colors.red,
                                              groupValue: _radioValued['${queston_index}-${index}'],
                                              onChanged: (val) {
                                                if(index == data.questions[queston_index]
                                                    .subQuestions.length - 1) {
                                                  _handleRadioValueChange1(
                                                      sub_question_item
                                                          .id,
                                                      val, index,
                                                      sub_question_item.audioUrl,
                                                      '',
                                                      false);
                                                }else{
                                                  _handleRadioValueChange1(
                                                      sub_question_item
                                                          .id,
                                                      val,
                                                      index,sub_question_item.audioUrl,
                                                      data.questions[queston_index]
                                                          .subQuestions[index+1].audioUrl,
                                                      true
                                                  );
                                                }
                                              },
                                            ),
                                          ),
                                          new Text(
                                            'Nej',
                                            style: new TextStyle(
                                                fontSize: 14.0),
                                          ),

                                        ],
                                      ),

                                    ],):
                                  Container(
                                    height: 95,
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                        ,
                      ):SizedBox(height: 0,)
                      ,
                      showNext && data
                          .questions[queston_index].subQuestions.length == index + 1 ?
                      Padding(
                          padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
                          child:
                          Material(
                            elevation: 5.0,

                            borderRadius: BorderRadius.circular(30.0),
                            color: Color(0xff3F458F),
                            child: MaterialButton(

                              key: Key('continue'),

                              onPressed: (){
                                if(data.questions.length <= queston_index+1 || examFinish){
                                  finishExam();
                                }else{
                                  // // print("----------```````````````````````~~~~~~~~~~~~~~~~~~----------```````````````````````~~~~~~~~~~~~~~~~~~");
                                  // // print(index);
                                  // // print("----------```````````````````````~~~~~~~~~~~~~~~~~~----------```````````````````````~~~~~~~~~~~~~~~~~~");
                                  // // print(question_list);
                                  nextQuestion();
                                }
                              },
                              minWidth: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                              child: Text( '${data.questions.length <= queston_index+1 || examFinish ? 'Finish Exam':'Continue'}',
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )
                      ) : SizedBox(height: 0,)
                    ],
                  ),
                );
              },
            ),
          ),


        ],
      );
  }
}
