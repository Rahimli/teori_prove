import 'dart:async';

import 'package:teori_prove/models/app-tariff-type.dart';
import 'package:teori_prove/models/result-detail.dart';
import 'package:teori_prove/models/results.dart';
import 'package:teori_prove/repository/provider.dart';
import 'package:teori_prove/models/category.dart';
import 'package:teori_prove/models/question.dart';

class Repository {
  final dataProvider = DataProvider();

  Future<AllResults> fetchAllStatistic() => dataProvider.fetchStatisticList();
  Future<CategoryData> fetchAllCategories() => dataProvider.fetchCategoryList();
  Future<AppTariffType> fetchAppTariffType() => dataProvider.fetchAppTariffType();

  Future<QuestionData> fetchQuestions(List<int> categoryIds) => dataProvider.fetchQuestion(categoryIds);
  Future<ResultQuestionData> fetchResultQuestions(String sessionId) => dataProvider.fetchResultQuestion(sessionId);
  Future<bool> fetchExamFinish(String sessionId,List<int> question_list,Map<String,int> my_answer_map) => dataProvider.fetchExamFinish(sessionId,question_list,my_answer_map);
}
