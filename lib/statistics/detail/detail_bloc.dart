import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:teori_prove/models/result-detail.dart';
import 'package:teori_prove/repository/repository.dart';
import 'package:teori_prove/models/question.dart';
//
//class StatisticDetailBloc {
//  final _repository = Repository();
//  final _sessionId = BehaviorSubject<String>();
//  final _questions = BehaviorSubject<Future<ResultQuestionData>>();
//
//  Function(String) get fetchSessionId => _sessionId.sink.add;
//  Observable<Future<ResultQuestionData>> get resultQuestions => _questions.stream;
//
//  StatisticDetailBloc() {
//    _sessionId.stream.transform(_itemTransformer()).pipe(_questions);
//  }
//
//  dispose() async {
//    _sessionId.close();
////    await _questions.drain();
////    _questions.close();
//  }
//
//  _itemTransformer() {
//    return ScanStreamTransformer(
//          (Future<ResultQuestionData> question, String session_id, int index) {
//        // print(index);
//        question = _repository.fetchResultQuestions(session_id);
//        return question;
//      },
//    );
//  }
//}
//
//
//



class StatisticDetailBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<ResultQuestionData> _subject =
  BehaviorSubject<ResultQuestionData>();

  getResults(String session_id) async {
    ResultQuestionData allResults = await _repository.fetchResultQuestions(session_id);
    // print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
    // print("--- AllResults -data ---");
    _subject.sink.add(allResults);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<ResultQuestionData> get subject => _subject;

}

final statistic_bloc = StatisticDetailBloc();