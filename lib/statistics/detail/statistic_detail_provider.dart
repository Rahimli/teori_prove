import 'package:flutter/material.dart';
import 'package:teori_prove/statistics/detail/detail_bloc.dart';



class StatisticDetailBlocProvider extends InheritedWidget {
  final StatisticDetailBloc bloc;

  StatisticDetailBlocProvider({Key key, Widget child})
      : bloc = StatisticDetailBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static StatisticDetailBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(StatisticDetailBlocProvider)
    as StatisticDetailBlocProvider)
        .bloc;
  }
}
