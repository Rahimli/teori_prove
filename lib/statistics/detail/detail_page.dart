import 'dart:async';


import 'package:flutter/material.dart';
import 'package:teori_prove/models/result-detail.dart';
import 'package:teori_prove/pages/loading.dart';
import 'package:teori_prove/pages/loading2.dart';
import 'package:teori_prove/pages/no-data.dart';




import 'package:teori_prove/statistics/detail/detail_bloc.dart';
import 'package:teori_prove/statistics/detail/statistic_detail_provider.dart';


class StatisticPageDetail extends StatefulWidget {
  final String session_id;


  const StatisticPageDetail({this.session_id});
  @override
  _StatisticPageDetailState createState() {
    return _StatisticPageDetailState(
      session_id:session_id,
    );
  }
}

class _StatisticPageDetailState extends State<StatisticPageDetail> {

  final String session_id;

  final PageController ctrl = PageController();
  _StatisticPageDetailState({this.session_id});

  StatisticDetailBloc bloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    bloc = StatisticDetailBlocProvider.of(context);
    // print("jjajjajjjajajajajajajjajjajajjajjajjajjjajajajajajajjajjajajja");
    // print(session_id);
    bloc.getResults(session_id);
    // print("jjajjajjj recreated");

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    bloc.dispose();
//    // print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    super.dispose();
//    // print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

  }

  ScrollController _scrollController = new ScrollController();


  void _onBackPressed(){}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body:
          StreamBuilder<ResultQuestionData>(
            stream: bloc.subject.stream,
            builder: (context, AsyncSnapshot<ResultQuestionData> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.error != null && snapshot.data.error.length > 0){
                  return ErrorData(snapshot.data.error);
                }
                return _buildMainWidget(snapshot.data);

              } else if (snapshot.hasError) {
                return ErrorData(snapshot.error);
              } else {
                return LoadingPage();
              }
            },
          )
        );
  }




  List<Widget> questionBox(String imageUrl, String text){
    return [
      Container(
        height: 200.0,

        decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage("${imageUrl}"),
              fit: BoxFit.fill,
            )
        ),
      ),
      // Image.network("${questionData.questions[currentIndex].imageUrl}",fit: BoxFit.fill,),
      Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10,20,10,20),
          child: Text("${text}"),
        ),
      ),
    ];
  }

  Widget _buildMainWidget(ResultQuestionData data) {
    return PageView.builder(
      controller: ctrl,
      itemCount: data
          .questions.length,
      itemBuilder: ((BuildContext context, int index){
        return
          Column(
            children: <Widget>[
              AppBar(
                title: Text("Question ${index+1}"),
              ),
              Column(
                children: questionBox(data
                    .questions[index].imageUrl,
                    data
                        .questions[index].text),
              ),

              Expanded(
                child: ListView.builder(
                  reverse: false,

                  shrinkWrap: true,
                  controller: _scrollController,
                  itemCount: data
                      .questions[index].subQuestions
                      .length,
                  itemBuilder: (BuildContext context,
                      int a_index) {

                    ResultAnswerQuestions sub_question_item = data.questions[index].subQuestions[a_index];
                    return Card(
                      color: sub_question_item.userAnswer == null ? Colors.white : sub_question_item.userAnswer == sub_question_item.answer ? Colors.green[100] : Colors.red[100],
                      child: Column(
                        children: <Widget>[
                          AnimatedOpacity(

                            opacity: 1.0,
                            duration: Duration(seconds: 2),
                            child:
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  10, 0, 10, 0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                // crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
//                                                          flex:84,
//                                                            width: MediaQuery.of(context).size.width * 0.70,
                                      child: Text("${sub_question_item.text}")
                                  ),
                                  Container(
                                    width: 85,
//                                                        Expanded(
//                                                          flex: 30,
                                    child: Padding(
                                        padding: const EdgeInsets.all(
                                            0),
                                        child: Column(
                                          // crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisAlignment: MainAxisAlignment
                                              .spaceEvenly,
                                          children: <Widget>[

                                            Row(
                                              crossAxisAlignment: CrossAxisAlignment
                                                  .center,
                                              children: <Widget>[
                                                Theme(
                                                  data: ThemeData(unselectedWidgetColor: Colors.green),
                                                  child: Radio(
                                                    value: 1,

                                                    activeColor: Colors.green,
                                                    groupValue: sub_question_item.userAnswer == null ? sub_question_item.answer : sub_question_item.userAnswer,
                                                    onChanged: (val) {
                                                    },
                                                  ),
                                                ),
                                                Text(
                                                  'Ja',
                                                  style: TextStyle(
                                                      fontSize: 14.0),
                                                ),

                                              ],
                                            ),
                                            Row(
                                              crossAxisAlignment: CrossAxisAlignment
                                                  .center,
                                              children: <Widget>[
                                                Theme(
                                                  data: ThemeData(unselectedWidgetColor: Colors.red),
                                                  child: Radio(
                                                    value: sub_question_item.userAnswer == null ? sub_question_item.answer : sub_question_item.userAnswer,
                                                    activeColor: Colors.red,
                                                    groupValue: 0,
                                                    onChanged: (val) {
                                                    },
                                                  ),
                                                ),
                                                new Text(
                                                  'Nej',
                                                  style: new TextStyle(
                                                      fontSize: 14.0),
                                                ),

                                              ],
                                            ),

                                          ],)
                                    ),
                                  )
                                ],
                              ),
                            )
                            ,
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),


            ],
          );;
      }),
    );
  }


}
