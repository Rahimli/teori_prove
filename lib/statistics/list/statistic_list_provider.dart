import 'package:flutter/material.dart';
import 'package:teori_prove/statistics/list/list_bloc.dart';



class StatisticBlocProvider extends InheritedWidget {
  final StatisticBloc bloc;

  StatisticBlocProvider({Key key, Widget child})
      : bloc = StatisticBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static StatisticBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(StatisticBlocProvider)
    as StatisticBlocProvider)
        .bloc;
  }
}
