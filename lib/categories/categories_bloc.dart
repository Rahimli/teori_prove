//import 'package:rxdart/rxdart.dart';
//import 'package:teori_prove/models/category.dart';
//import 'package:teori_prove/repository/repository.dart';
//
//class CategoriesBloc {
//  final Repository _repository = Repository();
//  final BehaviorSubject<CategoryData> _subject = BehaviorSubject<CategoryData>();
//
//
//  fetchAllCategories() async {
//    CategoryData categoryData = await _repository.fetchAllCategories();
//    // print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
//    // print("--- category category-data ---");
//    _subject.sink.add(categoryData);
//  }
//
//  dispose() {
//    _subject.close();
//  }
//
//  BehaviorSubject<CategoryData> get allCategories => _subject;
//}
//
//final categories_bloc = CategoriesBloc();


import 'package:teori_prove/models/category.dart';
import 'package:teori_prove/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class CategoriesBloc {
  final Repository _repository = Repository();
  final BehaviorSubject<CategoryData> _subject =
  BehaviorSubject<CategoryData>();

  getCategories() async {
    CategoryData categoryData = await _repository.fetchAllCategories();
//    // print("TEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEstTEst");
//    // print("--- category category-data ---");
    _subject.sink.add(categoryData);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<CategoryData> get subject => _subject;

}

final categories_bloc = CategoriesBloc();