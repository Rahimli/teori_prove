import 'package:flutter/material.dart';

class Loading2Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image.asset("assets/images/loader.gif"),
    );
  }
}