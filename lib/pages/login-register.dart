//import 'package:flutter/material.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:teori_prove/auth/authentication/authentication.dart';
//import 'package:teori_prove/auth/authentication/authentication_bloc.dart';
//import 'package:teori_prove/auth/authentication/authentication_event.dart';
//import 'package:teori_prove/auth/common/common.dart';
//import 'package:teori_prove/auth/login/login_page.dart';
//import 'package:teori_prove/auth/user_repository/signup_repository.dart';
//import 'package:teori_prove/home/home_page.dart';
//import 'package:teori_prove/pages/loading.dart';
//
//// import 'login/login_page.dart';
//// import 'package:teori_prove/pages/signin.dart';
//// import 'package:teori_prove/pages/signup.dart';
//
//class LoginRegister extends StatefulWidget {
//  @override
//  _LoginRegisterState createState() => _LoginRegisterState();
//}
//
//class _LoginRegisterState extends State<LoginRegister> {
//  AuthenticationBloc authenticationBloc;
//  UserRepository userRepository = UserRepository();
//
//  @override
//  void initState() {
//    authenticationBloc = AuthenticationBloc(userRepository: userRepository);
//    authenticationBloc.dispatch(AppStarted());
//    super.initState();
//  }
//
//  @override
//  void dispose() {
//    authenticationBloc.dispose();
//    super.dispose();
//  }
//
//
//
//  @override
//  Widget build(BuildContext context) {
//    return BlocProvider<AuthenticationBloc>(
//      bloc: authenticationBloc,
//      child: BlocBuilder<AuthenticationEvent, AuthenticationState>(
//          bloc: authenticationBloc,
//          builder: (BuildContext context, AuthenticationState state) {
//            if (state is AuthenticationUninitialized) {
//              return LoadingPage();
//            }
//            if (state is AuthenticationAuthenticated) {
//              return HomePage();
//            }
//            if (state is AuthenticationUnauthenticated) {
//              return LoginRegisterPage(userRepository: userRepository);
//            }
//            if (state is AuthenticationLoading) {
//              return LoadingIndicator();
//            }
//          },
//        ),
//    );
//  }
//
//}
//class LoginRegisterPage extends StatelessWidget {
//
//  final UserRepository userRepository;
//
//  const LoginRegisterPage({Key key, this.userRepository}) : super(key: key);
//
//
//  @override
//  Widget build(BuildContext context) {
//    final double statusbarHeight = MediaQuery
//        .of(context)
//        .padding
//        .top;
//
//    return Scaffold(
//      backgroundColor: Colors.purple,
//      body: Container(
//        padding: new EdgeInsets.only(top: statusbarHeight),
//        decoration: BoxDecoration(
//            image: DecorationImage(
//                image: AssetImage("assets/images/main_background1.png"),fit: BoxFit.cover
//            )
//        ),
//        child: Container(
//          decoration: BoxDecoration(
//              image: DecorationImage(
//                  image: AssetImage("assets/images/main_background.png"),fit: BoxFit.cover
//              )
//          ),
//          child: Stack(
//
//            children: <Widget>[
//              Align(
//                  alignment: Alignment(0, -0.85),
//                  child: Image.asset("assets/images/logo.png",height: 115,)
//              ),
//              Align(
//                  alignment: Alignment(0, 0.6),
//                  child: Container(
//                    child: Text("Text view", style: TextStyle(color: Colors.white),),
//                  )
//              ),
//              Align(
//                alignment: Alignment(0, 0.9),
//                child: Container(
//                  child: Row(
//                    crossAxisAlignment: CrossAxisAlignment.center,
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      RaisedButton(
//                        onPressed: (){
//                          Navigator.push(
//                            context,
//                            MaterialPageRoute(builder: (context) => LoginPage(userRepository: userRepository,)),
//                          );
//                          },
//                        child: Text("Sign in",style: TextStyle(color: Colors.deepPurple),),
//                        color: Colors.white, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),padding: EdgeInsets.all(14.0),
//                      ),
//                      SizedBox(width: 20.0,),
//                      RaisedButton(onPressed: (){
//                        // Navigator.push(
//                        //   context,
//                        //   MaterialPageRoute(builder: (context) => Signup()),
//                        // );
//                        },
//                        child: Text("Create Account",style: TextStyle(color: Colors.deepPurple)),color: Colors.white, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),padding: EdgeInsets.all(14.0),),
//                    ],
//                  ),
//                ),
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//}
