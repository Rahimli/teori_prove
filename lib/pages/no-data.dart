import 'package:flutter/material.dart';

class NoData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white
      ),
      child: Center(
        child: Text("Has no Data"),
      ),
    );
  }
}
class ErrorData extends StatelessWidget {
  final String error;

  const ErrorData(this.error);
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
//            Text("Error occured: $error"),
            Text("Has no data"),
          ],
        ));
  }
}