import 'package:meta/meta.dart';


class UserModel {
  int id;
  String name;
  String email;
  String paymentType;
  String accessToken;
  String tokenType;
  int expiresIn;

  UserModel(
      {
        this.id,
        this.name,
        this.email,
        this.paymentType,
        this.accessToken,
        this.tokenType,
        this.expiresIn
      });

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    paymentType = json['payment_type'];
    accessToken = json['access_token'];
    tokenType = json['token_type'];
    expiresIn = json['expires_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['payment_type'] = this.paymentType;
    data['access_token'] = this.accessToken;
    data['token_type'] = this.tokenType;
    data['expires_in'] = this.expiresIn;
    return data;
  }
}